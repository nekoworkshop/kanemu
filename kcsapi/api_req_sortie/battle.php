<?php
echo '{
    "api_result": 1,
    "api_result_msg": "成功",
    "api_data": {
        "api_dock_id": 1,
        "api_ship_ke": [-1, 510, 510, 505, 502, 502, -1],
        "api_ship_lv": [-1, 1, 1, 1, 1, 1, -1],
        "api_nowhps": [-1, 75, 43, 88, 13, 45, 45, 65, 65, 33, 22, 22, -1],
        "api_maxhps": [-1, 75, 43, 88, 14, 45, 45, 65, 65, 33, 22, 22, -1],
        "api_midnight_flag": 0,
        "api_eSlot": [
            [519, 523, 516, -1, -1],
            [519, 523, 516, -1, -1],
            [504, 525, -1, -1, -1],
            [502, -1, -1, -1, -1],
            [502, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1]
        ],
        "api_eKyouka": [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ],
        "api_fParam": [
            [39, 0, 79, 72],
            [63, 139, 49, 63],
            [98, 0, 63, 85],
            [9, 50, 0, 18],
            [29, 0, 39, 59],
            [22, 0, 39, 59]
        ],
        "api_eParam": [
            [0, 0, 15, 25],
            [0, 0, 15, 25],
            [14, 24, 10, 15],
            [7, 16, 7, 6],
            [7, 16, 7, 6],
            [0, 0, 0, 0]
        ],
        "api_search": [1, 1],
        "api_formation": [1, 3, 2],
        "api_stage_flag": [1, 1, 1],
        "api_kouku": {
            "api_plane_from": [
                [1, 2],
                [7, 8]
            ],
            "api_stage1": {
                "api_f_count": 175,
                "api_f_lostcount": 2,
                "api_e_count": 108,
                "api_e_lostcount": 57,
                "api_disp_seiku": 1,
                "api_touch_plane": [-1, -1]
            },
            "api_stage2": {
                "api_f_count": 150,
                "api_f_lostcount": 0,
                "api_e_count": 40,
                "api_e_lostcount": 12
            },
            "api_stage3": {
                "api_frai_flag": [-1, 0, 0, -1, -1, -1, -1],
                "api_erai_flag": [-1, 1, 1, 1, 1, 1, 0],
                "api_fbak_flag": [-1, 0, 1, -1, -1, -1, -1],
                "api_ebak_flag": [-1, 0, 1, 1, 1, 1, 0],
                "api_fcl_flag": [-1, 0, 0, 0, 0, 0, 0],
                "api_ecl_flag": [-1, 0, 0, 0, 0, 0, 0],
                "api_fdam": [-1, 0, 1, -1, -1, -1, -1],
                "api_edam": [-1, 52, 24.1, 74.1, 162, 32, 0]
            }
        },
        "api_support_flag": 0,
        "api_support_info": null,
        "api_opening_flag": 1,
        "api_opening_atack": {
            "api_frai": [-1, 1, 1, 0, 0, 0, 0],
            "api_erai": [-1, 0, 0, 0, 0, 0, 0],
            "api_fdam": [-1, 0, 0, 0, 0, 0, 0],
            "api_edam": [-1, 128, 0, 0, 0, 0, 0],
            "api_fydam": [-1, 106, 22, 0, 0, 0, 0],
            "api_eydam": [-1, 0, 0, 0, 0, 0, 0],
            "api_fcl": [-1, 0, 1, 0, 1, 0, 0],
            "api_ecl": [-1, 0, 0, 0, 0, 0, 0]
        },
        "api_hourai_flag": [1, 0, 0, 0],
        "api_hougeki1": {
            "api_at_list": [-1, 3],
            "api_at_type": [-1, 0],
            "api_df_list": [-1, [8]],
            "api_si_list": [-1, [133]],
            "api_cl_list": [-1, [1]],
            "api_damage": [-1, [97]]
        },
        "api_hougeki2": null,
        "api_hougeki3": null,
        "api_raigeki": null
    }
}';

?>