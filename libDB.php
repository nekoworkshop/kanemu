<?php
class DBO
{
    private static $port_id;
    private static $kdock_id;

    public static function getNextShipID(){
    	$dbConnection = new MongoClient(); // connects to localhost:27017
    	$db = $dbConnection->Globaldata;
    	$collection = $db->api_counter;
    	$nextID = $collection->findone(array(),array());
    	$nextShipID = $nextID['next_ship_ID'];
    	$nextID['next_ship_ID']++;
    	$collection->update(array(),$nextID);
    	return $nextShipID;
    }
    public static function getNextEquipmentID(){
    	$dbConnection = new MongoClient(); // connects to localhost:27017
    	$db = $dbConnection->Globaldata;
    	$collection = $db->api_counter;
    	$nextID = $collection->findone(array(),array());
    	$nextEquipmentID = $nextID['next_equipment_ID'];
    	$nextID['next_equipment_ID']++;
    	$collection->update(array(),$nextID);
    	return $nextEquipmentID;
    }
    
    public static function getPortData(){
    	$dbConnection = new MongoClient();
    	$db = $dbConnection->Globaldata;
    	$collection = $db->api_port->findone(array(),array());
    	
    	self::$port_id = $collection['_id'];
    	unset($collection['_id']);
    	return $collection;
    }
}

class KCSAPI{
	private static $shipCollection;
	private static $shipTemplateCollection;
	
	public static function req_kousyou_getShip($param){
		$DockID = $param["api_kdock_id"];
		
		$kDockCollection = new ConstructionDockCollection();
		$newShip = new ShipDataObject;
		$shipCollection = new ShipCollection();
		
		$newShipTemplateID = $kDockCollection->getShipTemplateIDInDock($DockID);
		$newShip->initializeFromTemplate($newShipTemplateID);
		$kDockCollection->releaseShipFromDock($DockID);
		$shipCollection->addShip($newShip);
		
		$responsePayload = array();
		$responsePayload['api_id'] = $newShip->getAPI_ID(); // need a function to get next id.
		$responsePayload['api_ship_id'] = $newShip->getAPI_Ship_ID();
		$responsePayload['api_kdock'] = $kDockCollection->getJSONData();
		$responsePayload['api_ship'] = $newShip->getJSONData();
		
		return $responsePayload;
	}
	
	public static function req_kousyou_createShip($param){
		$kDockCollection = new ConstructionDockCollection();
		$kDockCollection->contructShipInDock($param['api_kdock_id'], 
											rand(1,160), //new ship template ID
											10, 	//time needed
											$param['api_item1'], $param['api_item2'], $param['api_item3'], $param['api_item4'], $param['api_item5']);
		
		$materialObj = new MaterialDataObject();
		$materialObj->decreaseFuel($param['api_item1']);
		$materialObj->decreaseAmmo($param['api_item2']);
		$materialObj->decreaseSteel($param['api_item3']);
		$materialObj->decreaseAluminium($param['api_item4']);
		$materialObj->decreaseNails($param['api_item5']);
		$materialObj->update();
	}
	
	public static function req_kousyou_createitem($param){
		$newEquipment = new EquipmentDataObject();
		$newEquipment->newEquipmentFromTemplate(rand(1,138));
		$equipmentCollection = new EquipmentCollection();
		$unsetEquipmentCollection = new UnsetEquipmentCollection();
		$equipmentTemplateCollection = new EquipmentTemplateCollection();
		
		$equipmentCollection->addEquipment($newEquipment);
		$unsetEquipmentCollection->addUnsetEquipment($newEquipment->getAPIid(),$equipmentTemplateCollection->getEquipmentType3($newEquipment->getSlotItemid()));
		
		$result['api_create_flag'] = 1;
		$result['api_shizai_flag'] = 1;
		$result['api_slot_item'] = array( 'api_id' => $newEquipment->getAPIid() , 'api_slotitem_id' => $newEquipment->getSlotItemid());
		$result['api_type3'] = $equipmentTemplateCollection->getEquipmentType3($newEquipment->getSlotItemid());
		$result['api_unsetslot'] = $unsetEquipmentCollection->getUnsetEquipment($result['api_type3']);
		
		return $result;
		
	}
	
	public static function req_kousyou_destoryitem($param){
		$equipmentCollection = new EquipmentCollection();
		$unsetEquipmentCollection = new UnsetEquipmentCollection();
		$equipmentTemplateCollection = new EquipmentTemplateCollection();
		var_export($param['api_slotitem_ids']);
		foreach ($param['api_slotitem_ids'] as $key => $item){
			$equipmentCollection->deleteEquipment($item);
			$unsetEquipmentCollection->removeUnsetEquipment($item);
		}
		$result['api_get_material']= array(0,0,0,0);
		return $result;
	
	}
	
	public static function req_hensei_change($param){
		$deckDataCollection = new DeckDataCollection();
		if ($param['api_ship_id'] == -2){ 
			$deckDataCollection->unassignShipsExpectFlagship($param['api_id']-1);
		}else{
			$deckDataCollection->changeDeckConfiguration($param['api_ship_id'], $param['api_id']-1, $param['api_ship_idx']);
		}
	}
	
	public static function req_sortie_battle(){
		
	}
	
	public static function req_kaisou_slotset($param){
		$shipCollection = new ShipCollection();
		$unsetEquipmentCollection = new UnsetEquipmentCollection();
		$equipmentCollection = new EquipmentCollection();
		$equipmentTemplateCollection = new EquipmentTemplateCollection();
		
		//check whether putting on or taking off 
		if ($param['api_item_id'] != -1){
			$shipCollection->changeEquipment($param['api_id'], $param['api_item_id'], $param['api_slot_idx']);
			$unsetEquipmentCollection->removeUnsetEquipment($param['api_item_id']);
		}else{
			$oldEquipmentAPIID = $shipCollection->getShip($param['api_id'])['api_slot'][$param['api_slot_idx']];
			$shipCollection->changeEquipment($param['api_id'], $param['api_item_id'], $param['api_slot_idx']);
			$templateID = $equipmentCollection->getEquipmentTemplateID($oldEquipmentAPIID);
			$type3 = $equipmentTemplateCollection->getEquipmentType3($templateID);
			$unsetEquipmentCollection->addUnsetEquipment($oldEquipmentAPIID, $type3);
		}
	}
	
	public static function get_member_ship3($param){
		
		$deckCollection = new DeckDataCollection();
		$unsetEquipmentCollection = new UnsetEquipmentCollection();
		$shipCollection = new ShipCollection();
		
		
		$payload = array();
		$payload['api_ship_data'] = array($shipCollection->getShip($param['api_shipid']));
		$payload['api_deck_data'] = $deckCollection->getJSONData();
		$payload['api_slot_data'] = $unsetEquipmentCollection->getJSONData();
		
		return $payload;
	}
	
	public static function encodeResponseWithPayload($returnValue,$msg,array $payload){
		$apiResponse = array();
		$apiResponse["api_result"] = $returnValue;
		$apiResponse["api_result_msg"] = $msg;
		$apiResponse["api_data"] = $payload;
		$apiResponse = json_encode($apiResponse,JSON_UNESCAPED_UNICODE);
		return 'svdata='.$apiResponse;
	}
	public static function encodeResponseWithoutPayload($returnValue,$msg){
		$apiResponse = array();
		$apiResponse["api_result"] = $returnValue;
		$apiResponse["api_result_msg"] = $msg;
		$apiResponse = json_encode($apiResponse,JSON_UNESCAPED_UNICODE);
		return 'svdata='.$apiResponse;
	}
	
}

class BattleResult{
	private $api_dock_id;
	private $api_ship_ke; //emeny ships, 0 is reversed as -1
	private $api_ship_lv; //emeny ship lv, 0 is reversed as -1
	private $api_nowhps ; //13 elements array, blue 1 to 6, red 7 to 12. 0 is reversed as -1
	private $api_maxhps; //ditto
	private $api_midnight_flag; //川内「！」
	private $api_eSlot; // blue force equipments.
	private $api_eKyouka; // probably for attributes.
	private $api_fParam; //blue force attributes.
	private $api_eParam; //red force attributes.
	private $api_search; //???
	private $api_formation; //array ???
	private $api_stage_flag; //array ???
	private $api_kouku; //Air combat and naval strike.
	private $api_support_flag ;
	private $api_support_info ;
	private $api_opening_flag;
	private $api_opening_atack; //typo?
	private $api_hourai_flag;
	private $api_hougeki1;
	private $api_hougeki2;
	private $api_hougeki3;
	private $api_raigeki;
	
	
	private function resolveAttackOrder(){
		
	}
}

//api_start2
class WorldData{
    
}

//api_port
class PortDataObject{
    private $dbConnection;
    private $db;
    private $_id;
    
    public function __construct() {
    	$i = $this->getCollectionsfromDB();
    	foreach ($i as $key => $item){
    		//$this->constructionDocks[$key] = new ConstructionDock($item);
    	}
    }

    	private function writeCollectionsintoDB(){
    		if (!empty($this->constructionDocks)){
    			$dbConnection = new MongoClient();
    			$db = $dbConnection->Globaldata;
    			$collection = $db->api_kdock;
    			$data = $this->getJSONData();
    			$data['_id'] = $this->_id;
    			$collection->update(array("_id" => $this->_id),$data);
    		}else{
    			die();
    		}
    	}
    	
    	//returns the database collection as an array.
    	private function getPortDatafromDB(){
    		$dbConnection = new MongoClient();
    		$db = $dbConnection->Globaldata;
    		$portdata = $db->api_port->findone(array(),array());
    		$this->_id = $portdata['_id'];
    		unset($portdata['_id']);
    		return ($portdata);
    	}
}

class MaterialDataObject{
	private $materials = array();
	
	function __construct(){
		$dbConnection = new MongoClient(); // connects to localhost:27017
		$db = $dbConnection->Globaldata;
		$playerDataCollection = $db->api_port;
		$playerData = $playerDataCollection->findone(array());
		$this->initializeFromJSON($playerData['api_material']);
	}
	
	public function update(){
		$dbConnection = new MongoClient(); // connects to localhost:27017
		$db = $dbConnection->Globaldata;
		$playerDataCollection = $db->api_port;
		$playerData = $playerDataCollection->findone(array());
		$playerData['api_material'] = $this->getJSONData();
		$playerDataCollection->update(array("_id" => $playerData['_id']),$playerData);
	}
	
	public function initializeFromJSON(array $data) {
		foreach($data as $key => $val){
			$this->materials[$key] = $val;
		}
	}
	public function getJSONData(){
		return $this->materials;
	}
	
	public function increaseFuel($amount){
		$this->materials[0]['api_value'] += $amount;
	}
	public function increaseAmmo($amount){
		$this->materials[1]['api_value'] += $amount;
	}
	public function increaseSteel($amount){
		$this->materials[2]['api_value'] += $amount;
	}
	public function increaseAluminium($amount){
		$this->materials[3]['api_value'] += $amount;
	}
	public function increaseBurners($amount){
		$this->materials[4]['api_value'] += $amount;
	}
	public function increaseBuckets($amount){
		$this->materials[5]['api_value'] += $amount;
	}
	public function increaseNails($amount){
		$this->materials[6]['api_value'] += $amount;
	}
	public function increaseScrews($amount){
		$this->materials[7]['api_value'] += $amount;
	}
	
	public function decreaseFuel($amount){
		$this->materials[0]['api_value'] -= $amount;
	}
	public function decreaseAmmo($amount){
		$this->materials[1]['api_value'] -= $amount;
	}
	public function decreaseSteel($amount){
		$this->materials[2]['api_value'] -= $amount;
	}
	public function decreaseAluminium($amount){
		$this->materials[3]['api_value'] -= $amount;
	}
	public function decreaseBurners($amount){
		$this->materials[4]['api_value'] -= $amount;
	}
	public function decreaseBuckets($amount){
		$this->materials[5]['api_value'] -= $amount;
	}
	public function decreaseNails($amount){
		$this->materials[6]['api_value'] -= $amount;
	}
	public function decreaseScrews($amount){
		$this->materials[7]['api_value'] -= $amount;
	}
}

class DeckDataObject{
	
	private $api_member_id;
	private $api_id;
	private $api_name;
	private $api_name_id;
	private $api_mission;
	private $api_flagship;
	private $api_ship;
	
	public function initializeFromJSON(array $data) {
		foreach($data as $key => $val){
			if(property_exists($this,$key)){
				$this->$key = $val;
			}
		}
	}
	
	public function changeShipAtIdx ($shipAPIID,$idx){
		$oldShip = $this->api_ship[$idx]; 
		$this->api_ship[$idx] = $shipAPIID;
		$this->compress();
		return $oldShip;
	}
	
	public function removeExceptFlagship(){
		for ($i=1;$i<=5;$i++){
			$this->api_ship[$i] = -1;
		}
	}
	
	public function findShipByID ($shipAPIID){
		foreach ($this->api_ship as $key => $item){
			if($item == $shipAPIID){
				return $key;
			}
		}
		return -1;
	}
	
	public function getShipByIdx($idx){
		return $this->api_ship['idx'];
	}
	
	public function compress(){
		$compressed = array(-1,-1,-1,-1,-1,-1);
		$pivot = 0;
		for ($i=0;$i<=5;$i++){
			if ($this->api_ship[$i] != -1){
				$compressed[$pivot] = $this->api_ship[$i];
				$pivot++;
			}
		}
		$this->api_ship = $compressed;
		//var_export($compressed);
	}
	
	public function getJSONData(){
		return get_object_vars($this);
	}
}

class DeckDataCollection{
	private $decks = array();
	private $portData;
	
	public function __construct() {
		$i = $this->getCollectionsfromDB();
		foreach ($i as $key => $item){
			$this->decks[$key] = new DeckDataObject();
			$this->decks[$key]->initializeFromJSON($item);
		}
	}
	
	public function changeDeckConfiguration ($shipAPIID,$deckIdx,$shipIdx){
		$sourceShipLocation = $this->findShipInDeckCollection($shipAPIID);
		//var_export($sourceShipLocation);
		
		if ($shipAPIID == -1){
			$this->decks[$deckIdx]->changeShipAtIdx(-1, $shipIdx);
			$this->writeCollectionsintoDB();
			return;
		}
		
		if ($sourceShipLocation == -1){
			//The new ship is not in the deck.
			$this->decks[$deckIdx]->changeShipAtIdx($shipAPIID, $shipIdx);
		}else{
			//The new ship is in the deck. We need to swap them.
			$replacedShip =$this->decks[$deckIdx]->changeShipAtIdx($shipAPIID, $shipIdx);
			$this->decks[$sourceShipLocation['deckIdx']]->changeShipAtIdx($replacedShip, $sourceShipLocation['inDeckIdx']);
		}
		$this->writeCollectionsintoDB();
	}
	
	public function unassignShipsExpectFlagship ($deckIdx){
		$this->decks[$deckIdx]->removeExceptFlagship();
		$this->writeCollectionsintoDB();
	}
	
	public function getJSONData(){
		$var = array();
		foreach($this->decks as $key => &$value){
			$var[$key] = $value->getJSONData();
		}
		return $var;
	}
	
	public function findShipInDeckCollection ($shipAPIID){
		foreach ($this->decks as $key => $item){
			if ($item->findShipByID($shipAPIID) != -1){
				return array('deckIdx' => $key, 'inDeckIdx' => $item->findShipByID($shipAPIID));
			}
		}
		return -1;
	}
	
	private function writeCollectionsintoDB(){
		$dbConnection = new MongoClient();
		$collection = $dbConnection->Globaldata->api_port;
		$this->portData['api_deck_port'] = $this->getJSONData();
		$collection->update(array("_id" => $this->portData['_id']),$this->portData);
	}
	
	//returns the database collection as an array.
	private function getCollectionsfromDB(){
		$dbConnection = new MongoClient();
		$this->portData = $dbConnection->Globaldata->api_port->findone(array(),array());
		return $this->portData['api_deck_port'];
	}
}



class EquipmentCollection{
	private $slot_items = array();
	private $_id;
	
	function __construct(){
		$i = $this->getCollectionsfromDB();
		
		foreach ($i as $key => $item){
			$this->slot_items[$key] = new EquipmentDataObject();
			$this->slot_items[$key]->initializeFromArray($item);
		}
		//var_export($this->slot_items);
	}
	
	public function addEquipment(&$newEquipment){
		array_push($this->slot_items, $newEquipment);
		$this->writeCollectionsintoDB();
	}
	
	public function deleteEquipment($EquipmentID){
		foreach ($this->slot_items as $key => $item){
			if($item->getAPIid() == $EquipmentID){
				array_splice($this->slot_items,$key,1);
			}
		}
		$this->writeCollectionsintoDB();
	}
	
	public function getEquipmentTemplateID($EquipmentID){
		foreach ($this->slot_items as $key => $item){
			if($item->getAPIid() == $EquipmentID){
				return $item->getSlotItemid();
			}
		}
	}
	
	public function lockEquip($equipID){
		
	}
	public function unlockEquip($equipID){
		
	}
	public function upgradeEquip($equipID){
		
	}
	
	public function getJSONData(){
		$var = array();
		foreach($this->slot_items as $key => &$value){
			$var[$key] = $value->getJSONData();
		}
		return $var;
	}
	
	private function writeCollectionsintoDB(){
		$dbConnection = new MongoClient();
		$collection = $dbConnection->Globaldata->api_slot_item;
		$newData = $this->getJSONData();
		$newData['_id'] = $this->_id;
		$collection->update(array("_id" => $this->_id),$newData);
	}
	
	private function getCollectionsfromDB(){
		$dbConnection = new MongoClient();
		$collection = $dbConnection->Globaldata->api_slot_item->findone(array(),array());
		$this->_id = $collection['_id'];
		unset($collection['_id']);
		return $collection;
	}
}

class EquipmentTemplateCollection{
	private $templates = array();
	
	function __construct(){
		$i = $this->getCollectionsfromDB();
		foreach ($i as $key => $item){
			$this->templates[$key] = new EquipmentTemplate($item);
		}
	}
	public function getEquipmentType3 ($Templateid){
		foreach ($this->templates as $key => $item){
			if($item->getID() == $Templateid){
				return $item->getType3();
			}
		}
	}
	
	
	
	//There is no need to modify the template database.
	//private function writeCollectionsintoDB()
	
	private function getCollectionsfromDB(){
		$dbConnection = new MongoClient();
		return $dbConnection->Globaldata->api_mst_slotitem->find(array(),array('_id' => false));
	}
	
}


class UnsetEquipmentCollection{
	private $slot_itemData;
	private $equipmentTemplates;
	//private

	function __construct(){
		$this->getCollectionsfromDB();
		$this->equipmentTemplates = new EquipmentTemplateCollection();
	}

	public function addUnsetEquipment($equipmentID,$Type3){
		if ($this->slot_itemData['api_slottype'.$Type3] == -1){
			$this->slot_itemData['api_slottype'.$Type3] = array($equipmentID);
		}else{
			array_push($this->slot_itemData['api_slottype'.$Type3],$equipmentID);
		}
		$this->writeCollectionsintoDB();
	}
	
	public function removeUnsetEquipment($equipmentID){
		foreach ($this->slot_itemData as $key => &$item){
			if (is_array($item)){
				foreach ($item as $key_ => &$item_){
					var_export($item_);
					if ($item_ == $equipmentID){
						array_splice($item, $key_,1);
						echo 'after splice = ';
						var_export($item_);
						if (empty($item)){
							$item = -1;
							break;
						}
					}
				}
			}
		}
		$this->writeCollectionsintoDB();
	}
	
	public function getUnsetEquipment ($typeID){
		return $this->slot_itemData['api_slottype'.$typeID];
	}
	
	public function getJSONData(){
		return  $this->slot_itemData;
	}
	
	private function getCollectionsfromDB(){
		$dbConnection = new MongoClient();
		$this->slot_itemData = $dbConnection->Globaldata->api_unsetslot->findone(array(),array('_id' => false));
	}
	
	private function writeCollectionsintoDB(){
		$dbConnection = new MongoClient();
		$collection = $dbConnection->Globaldata->api_unsetslot;
		$collection->update(array(),$this->slot_itemData);
	}
	
}


class EquipmentDataObject{
	private $data = array();
	
	function __construct(){
	}
	
	public function initializeFromArray($data){
		$this->data = $data; 
	}
	
	public function newEquipmentFromTemplate($templateID){
		$this->data['api_id'] = DBO::getNextEquipmentID();
		$this->data['api_slotitem_id'] = $templateID;
		$this->data['api_locked'] = 0;
		$this->data['api_level'] = 0; //may try random level.
	}
	
	public function getAPIid(){
		return $this->data['api_id'];
	}
	
	public function getSlotItemid(){
		return $this->data['api_slotitem_id'];
	}
	
	public function getJSONData(){
		return $this->data;
	}
}

class EquipmentTemplate{
	private $properties = array();
	public function __construct($data){
		foreach($data as $key => $item){
			$this->properties[$key] = $item;
		}
	}
	//There are four type value in equipment template. 
	//For now this one is being used in equipment panel.
	public function getType3(){
		return $this->properties['api_type'][2];
	}
	public function getID(){
		return $this->properties['api_id'];
	}
}

class ShipCollection{
	private $ships = array();
	private $portData;
	
	public function __construct() {
		$i = $this->getCollectionsfromDB();
		foreach ($i as $key => $item){
			$this->ships[$key] = new ShipDataObject();
			$this->ships[$key]->initializeFromJSON($item);
		}
	}
	
	public function addShip($newShip){
		array_push($this->ships,$newShip);
		$this->writeCollectionsintoDB();
	}
	
	public function removeShip($shipToRemove){
		//TODO: Need to change index after this ship.
		//May use array_splice.
	}
	
	public function getShip($shipAPIID){
		foreach ($this->ships as $key => $item ){
			if($item->getAPI_ID() == $shipAPIID){
				return $item->getJSONData();
			}
		}
	}
	
	public function changeEquipment ($shipAPIID,$equipmentAPIID,$slotNumber){
		foreach ($this->ships as $key => $item ){
			if($item->getAPI_ID() == $shipAPIID){
				$item->setEquipmentOnSlot($slotNumber, $equipmentAPIID);
			}
		}
		$this->writeCollectionsintoDB();
	}
	
	
	public function getJSONData(){
		$var = array();
		foreach($this->ships as $key => &$value){
			$var[$key] = $value->getJSONData();
		}
		return $var;
	}
	
	private function writeCollectionsintoDB(){
		$dbConnection = new MongoClient();
		$collection = $dbConnection->Globaldata->api_port;
		$this->portData['api_ship'] = $this->getJSONData();
		$collection->update(array("_id" => $this->portData['_id']),$this->portData);
	}
	
	//returns the database collection as an array.
	private function getCollectionsfromDB(){
		$dbConnection = new MongoClient();
		$this->portData = $dbConnection->Globaldata->api_port->findone(array(),array());
		return $this->portData['api_ship'];
	}
}

//api_ship
class ShipDataObject{
    private $api_id; //This is the unique ID for this ship instance.
    private $api_sortno;
    private $api_ship_id;
    private $api_lv;
    private $api_exp;
    private $api_nowhp;
    private $api_maxhp;
    private $api_leng; //range
    private $api_slot; //TODO need unique id for new equipment
    private $api_onslot; //TODO naval aircraft capacity
    private $api_kyouka;
    private $api_backs;
    private $api_fuel;
    private $api_bull;
    private $api_slotnum;
    private $api_ndock_time; //repair time??
    private $api_ndock_item; //???
    private $api_srate; //???
    private $api_cond;
    private $api_karyoku;
    private $api_raisou;
    private $api_taiku;
    private $api_soukou;
    private $api_kaihi; //TODO didn't appear in app_start JSON.
    private $api_taisen; //TODO didn't appear in app_start JSON.
    private $api_sakuteki; // TODO didn't appear in app_start JSON.
    private $api_lucky; 
    private $api_locked;
    private $api_locked_equip;
    private $api_sally_area;
    
    public function initializeFromJSON(array $data) {
        foreach($data as $key => $val){
            if(property_exists($this,$key)){
                $this->$key = $val;
            }
        }
    }
    function __construct() {
        //意味ねえｗｗ
    }
    //Initialize by templateID. TODO
    public function initializeFromTemplate($templateID) {
        $dbConnection = new MongoClient(); // connects to localhost:27017
        $db = $dbConnection->Globaldata;
        $shipDataCollection = $db->api_mst_ship;
        $shipTemplate = $shipDataCollection->findone(array('api_id' => $templateID),array('_id' => false));
        $this->api_id = DBO::getNextShipID(); //TODO aquire a unique id for this ship instance.
        $this->api_sortno = $shipTemplate ['api_sortno'];
        $this->api_ship_id = $shipTemplate ['api_id'];
        $this->api_lv = 1;
        $this->api_exp = array(0,100,0);
        $this->api_nowhp = $shipTemplate ['api_taik'][0];
        $this->api_maxhp = $shipTemplate ['api_taik'][0];
        $this->api_leng = $shipTemplate ['api_leng']; //range
        $this->api_slot = array(-1,-1,-1,-1,-1); //TODO need unique id for new equipment
        $this->api_onslot = array(0,0,0,0,0); //TODO naval aircraft capacity
        $this->api_kyouka = array(0,0,0,0,0);
        $this->api_backs = $shipTemplate ['api_backs'];
        $this->api_fuel = $shipTemplate ['api_fuel_max'];
        $this->api_bull = $shipTemplate ['api_bull_max'];
        $this->api_slotnum = $shipTemplate ['api_slot_num'];
        $this->api_ndock_time = 0; //repair time??
        $this->api_ndock_item = array(0,0); //???
        $this->api_srate = 0; //???
        $this->api_cond = 40;
        $this->api_karyoku = $shipTemplate ['api_houg'];
        $this->api_raisou = $shipTemplate ['api_raig'];
        $this->api_taiku = $shipTemplate ['api_tyku'];
        $this->api_soukou = $shipTemplate ['api_souk'];
        $this->api_kaihi = array(15,30); //TODO didn't appear in app_start JSON.
        $this->api_taisen = array(15,30); //TODO didn't appear in app_start JSON.
        $this->api_sakuteki = array(15,30); // TODO didn't appear in app_start JSON.
        $this->api_lucky = $shipTemplate ['api_luck']; 
        $this->api_locked = 0;
        $this->api_locked_equip = 0;
        $this->api_sally_area = 0;
        
        $this->
        //It makes sense that app_start didn't return all properties of a ship template since there is no need for the client to check those values (luck, ASW, dodge, etc).
        //currently missing : initial equipments, dodge, luck, ASW and detection.
    }
    
    public function getJSONData(){
        $var = get_object_vars($this);
        foreach($var as &$value){
           if(is_object($value) && method_exists($value,'getJsonData')){
              $value = $value->getJsonData();
           }
        }
        return $var;
    }
    
    public function getAPI_ID(){
        return $this->api_id;
    }
    public function getAPI_Ship_ID(){
        return $this->api_ship_id;
    }
    
    public function setEquipmentOnSlot($slotIndex,$equipmentAPIID){
    	$this->api_slot[$slotIndex] = $equipmentAPIID;
    }
    
    private function reCalculateAttributes(){
    	
    }
    
    private function capAttributes(){
    	 
    }
}


class ShipTemplate{
	private $properties = array();
	
	public function __construct($data){
		foreach($data as $key => $item){
			$this->properties[$key] = $item;
		}
	}
	public function getID(){
		return $this->properties['api_id'];
	}
	
	public function getMaxTaiku(){
		
	}
	
	public function getMaxRaisou(){
	
	}
	
	public function getMaxKaryoku(){
	
	}
	
	public function getMaxSoukou(){
	
	}
	
	public function getMaxKaihi(){
	
	}
	
	public function getMaxTaisen(){
	
	}
	
	public function getMaxSakuteki(){
	
	}
	
	public function getMaxLucky(){
	
	}
	
	public function getSlotCount(){
	
	}

	public function getPowerUp(){
	
	}
	public function getJSONData(){
		return $this->properties;
	}
}

class ShipTemplateCollection{
	private $templates = array();
	
	function __construct(){
		$i = $this->getCollectionsfromDB();
		foreach ($i as $key => $item){
			$this->templates[$key] = new ShipTemplate($item);
		}
	}
	public function getEquipmentType3 ($Templateid){
		foreach ($this->templates as $key => $item){
			if($item->getID() == $Templateid){
				return $item->getType3();
			}
		}
	}
	
	public function getTemplate ($Templateid){
		foreach ($this->templates as $key => $item){
			if($item->getID() == $Templateid){
				return $item->getJSONData();
			}
		}
	}
	
	
	//private function writeCollectionsintoDB()
	//There is no need to modify the template database.
	private function getCollectionsfromDB(){
		$dbConnection = new MongoClient();
		return $dbConnection->Globaldata->api_mst_ship->find(array(),array('_id' => false));
	}
	
}

//A.K.A kdock. Really wanted to call them Konstruction docks though
/*
 * 
	api_member_id	整数	提督のID
	api_id	整数	建造ドックナンバー(1,2,3,4)
	api_state	整数	ドックの状態
		[-1]未解放
		[0]空き
		[2]建造中
		[3]建造完了
	api_created_ship_id	整数	建造した艦のID
	api_complete_time	整数	建造完了時間(epoch time in microseconds)
	api_complete_time_str	文字列	建造完了時間
		format:yyyy-mm-dd hh:mm:ss
	api_item1	整数	投入した燃料量
	api_item2	整数	投入した弾薬量
	api_item3	整数	投入した鋼材量
	api_item4	整数	投入したボーキサイト量
	api_item5	整数	投入した開発資材量
 */
class ConstructionDock{
    private $api_member_id;
    private $api_id;
    private $api_state;
    private $api_created_ship_id;
    private $api_complete_time;
    private $api_complete_time_str;
    private $api_item1;
    private $api_item2;
    private $api_item3;
    private $api_item4;
    private $api_item5;
    
    function __construct(array $data){
    	foreach($data as $key => $val){
    		if(property_exists($this,$key)){
    			$this->$key = $val;
    		}
    	}
    }
    
    public function getJSONData(){
    	$var = get_object_vars($this);
    	foreach($var as &$value){
    		if(is_object($value) && method_exists($value,'getJSONData')){
    			$value = $value->getJsonData();
    		}
    	}
    	return $var;
    }
    
    public function resetDock(){
    	$this->api_state=0;
    	$this->api_created_ship_id=0;
    	$this->api_complete_time=0;
    	$this->api_complete_time_str=0;
    	$this->api_item1=0;
    	$this->api_item2=0;
    	$this->api_item3=0;
    	$this->api_item4=0;
    	$this->api_item5=1;
    }
    
	public function getApiMemberId() {
		return $this->api_member_id;
	}
	public function setApiMemberId($api_member_id) {
		$this->api_member_id = $api_member_id;
	}
	public function getApiId() {
		return $this->api_id;
	}
	public function setApiId($api_id) {
		$this->api_id = $api_id;
	}
	public function getApiState() {
		return $this->api_state;
	}
	public function setApiState($api_state) {
		$this->api_state = $api_state;
	}
	public function getApiCreatedShipId() {
		return $this->api_created_ship_id;
	}
	public function setApiCreatedShipId($api_created_ship_id) {
		$this->api_created_ship_id = $api_created_ship_id;
	}
	public function getApiCompleteTime() {
		return $this->api_complete_time;
	}
	public function setApiCompleteTime($api_complete_time) {
		$this->api_complete_time = $api_complete_time;
	}
	public function getApiCompleteTimeStr() {
		return $this->api_complete_time_str;
	}
	public function setApiCompleteTimeStr($api_complete_time_str) {
		$this->api_complete_time_str = $api_complete_time_str;
	}
	public function getApiItem1() {
		return $this->api_item1;
	}
	public function setApiItem1($api_item1) {
		$this->api_item1 = $api_item1;
	}
	public function getApiItem2() {
		return $this->api_item2;
	}
	public function setApiItem2($api_item2) {
		$this->api_item2 = $api_item2;
	}
	public function getApiItem3() {
		return $this->api_item3;
	}
	public function setApiItem3($api_item3) {
		$this->api_item3 = $api_item3;
	}
	public function getApiItem4() {
		return $this->api_item4;
	}
	public function setApiItem4($api_item4) {
		$this->api_item4 = $api_item4;
	}
	public function getApiItem5() {
		return $this->api_item5;
	}
	public function setApiItem5($api_item5) {
		$this->api_item5 = $api_item5;
	}
}

class ConstructionDockCollection{
	private $constructionDocks = array();
	private $_id;
	
	public function __construct() {
		$i = $this->getCollectionsfromDB();
		foreach ($i as $key => $item){
			$this->constructionDocks[$key] = new ConstructionDock($item);
		}
	}
	public function getJSONData(){
		$var = array();
		foreach($this->constructionDocks as $key => &$value){
			$var[$key] = $value->getJSONData();
		}
		return $var;
	}
	
	public function getShipTemplateIDInDock($dockID){
		return $this->constructionDocks[intval($dockID-1)]->getApiCreatedShipId();
	}
	
	public function releaseShipFromDock($dockID){
		//This will update the dock properties.
		$this->constructionDocks[$dockID-1]->resetDock();
		$this->writeCollectionsintoDB();
	}
	
	public function contructShipInDock($dockID,$newShipTemplateID,$needTime,$fuel,$ammo,$steel,$aluminium,$nails){
		$this->constructionDocks[$dockID-1]->setApiState(2);
		$this->constructionDocks[$dockID-1]->setApiCreatedShipId($newShipTemplateID);
		$this->constructionDocks[$dockID-1]->setApiCompleteTime(time() + $needTime);
		$this->constructionDocks[$dockID-1]->setApiItem1($fuel);
		$this->constructionDocks[$dockID-1]->setApiItem2($ammo);
		$this->constructionDocks[$dockID-1]->setApiItem3($steel);
		$this->constructionDocks[$dockID-1]->setApiItem4($aluminium);
		$this->constructionDocks[$dockID-1]->setApiItem5($nails);
		$this->writeCollectionsintoDB();
	}
	
	private function writeCollectionsintoDB(){
		if (!empty($this->constructionDocks)){
			$dbConnection = new MongoClient();
			$db = $dbConnection->Globaldata;
			$collection = $db->api_kdock;
 			$data = $this->getJSONData();
			$data['_id'] = $this->_id;
			$collection->update(array("_id" => $this->_id),$data);
		}else{
			die();
		}
	}
	
	//returns the database collection as an array.
	private function getCollectionsfromDB(){
		$dbConnection = new MongoClient();
		$db = $dbConnection->Globaldata;
		$collection = $db->api_kdock->findone(array(),array());
		$this->_id = $collection['_id'];
		unset($collection['_id']);
		return ($collection);
	}
}


function getKdock(){
    $dbConnection = new MongoClient; // connects to localhost:27017
    $db = $dbConnection->Globaldata;
    $kdockDataCollection = $db->api_kdock;
    return $kdockDataCollection->findone(array(),array('_id' => false));
}

function appendResponseHeader($result,$msg,array $payload){
    $apiResponse = array();
    $apiResponse["api_result"] = 1;
    $apiResponse["api_result_msg"] = "成功";
    $apiResponse["api_data"] = $payload;
    $apiResponse = json_encode($apiResponse,JSON_UNESCAPED_UNICODE);
    return 'svdata='.$apiResponse;
}

function addShipIntoInventory(){
    
}

function addEquipmentIntoInventory(){
    
}

//returns a 'empty' ship instance based on template ID. Don't forget to give her some equipments unless you want to see her all naked :3
function generateNewShip ($shipID){
    $dbConnection = new MongoClient(); // connects to localhost:27017
    $db = $dbConnection->Globaldata;
    $shipDataCollection = $db->api_mst_ship;
    $shipTemplate = $shipDataCollection->findone(array('api_id' => $shipID),array('_id' => false));
    $newShip = array();
    $newShip['api_id'] = 100; //unique id for a ship instance.
    $newShip['api_sortno'] = $shipTemplate ['api_sortno'];
    $newShip['api_ship_id'] = $shipTemplate ['api_id'];
    $newShip['api_lv'] = 1;
    $newShip['api_exp'] = array(0,100,0);
    $newShip['api_nowhp'] = $shipTemplate ['api_taik'][0];
    $newShip['api_maxhp'] = $shipTemplate ['api_taik'][0];
    $newShip['api_leng'] = $shipTemplate ['api_leng']; //range
    $newShip['api_slot'] = array(0,0,0,0,0); //TODO need unique id for new equipment
    $newShip['api_onslot'] = array(0,0,0,0,0); //TODO naval aircraft capacity
    $newShip['api_kyouka'] = array(0,0,0,0,0);
    $newShip['api_backs'] = $shipTemplate ['api_backs'];
    $newShip['api_fuel'] = $shipTemplate ['api_fuel_max'];
    $newShip['api_bull'] = $shipTemplate ['api_bull_max'];
    $newShip['api_slotnum'] = $shipTemplate ['api_slot_num'];
    $newShip['api_ndock_time'] = 0; //repair time??
    $newShip['api_ndock_item'] = array(0,0); //???
    $newShip['api_srate'] = 0; //???
    $newShip['api_cond'] = 40;
    $newShip['api_karyoku'] = $shipTemplate ['api_houg'];
    $newShip['api_raisou'] = $shipTemplate ['api_raig'];
    $newShip['api_taiku'] = $shipTemplate ['api_tyku'];
    $newShip['api_soukou'] = $shipTemplate ['api_souk'];
    $newShip['api_kaihi'] = 15; //TODO didn't appear in app_start JSON.
    $newShip['api_taisen'] = 15; //TODO didn't appear in app_start JSON.
    $newShip['api_sakuteki'] = 4; // TODO didn't appear in app_start JSON.
    $newShip['api_lucky'] = $shipTemplate ['api_luck']; 
    $newShip['api_locked'] = 0;
    $newShip['api_locked_equip'] = 0;
    $newShip['api_sally_area'] = 0;
    
    return $newShip;
    //It makes sense that app_start didn't return all properties of a ship template since there is no need for the client to check those values (luck, ASW, dodge, etc).
    //currently missing : initial equipments, dodge, luck, ASW and detection.
}


?>